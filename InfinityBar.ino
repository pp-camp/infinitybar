#include <FastLED.h>

#define NUM_SEGMENTS 10
#define LEDS_PER_SEGMENT 45
#define NUM_LEDS (NUM_SEGMENTS*LEDS_PER_SEGMENT)

// For led chips like Neopixels, which have a data line, ground, and power, you just
// need to define DATA_PIN.  For led chipsets that are SPI based (four wires - data, clock,
// ground, and power), like the LPD8806, define both DATA_PIN and CLOCK_PIN
#define DATA_PIN 3

// Define the array of leds
CRGB leds[NUM_LEDS];

void setup() { 
	Serial.begin(115200);
	Serial.println("resetting");
	FastLED.addLeds<WS2812,DATA_PIN,RGB>(leds,NUM_LEDS);
	//FastLED.setBrightness(84);
  FastLED.setBrightness(255);
}

void fadeall() { for(int i = 0; i < NUM_LEDS; i++) { leds[i].nscale8(254); } }

void loop() { 
	static uint8_t hue = 0;
	Serial.print("x");
	// First slide the led in one direction
	for(int i = 0; i < LEDS_PER_SEGMENT; i++) {
		for (int j = 0; j < NUM_SEGMENTS; j++) {
		  leds[i+j*LEDS_PER_SEGMENT] = CHSV(hue, 255, 255);
		}
    hue++;
		// Show the leds
		FastLED.show(); 
		// now that we've shown the leds, reset the i'th led to black
		// leds[i] = CRGB::Black;
		fadeall();
		// Wait a little bit before we loop around and do it again
		delay(10);
	}
//	Serial.print("x");
//
//	// Now go in the other direction.  
//	for(int i = (LEDS_PER_SEGMENT)-1; i >= 0; i--) {
//    for (int j = 0; j< NUM_SEGMENTS; j++
//		leds[i] = CHSV(hue++, 255, 255);
//		// Show the leds
//		FastLED.show();
//		// now that we've shown the leds, reset the i'th led to black
//		// leds[i] = CRGB::Black;
//		fadeall();
//		// Wait a little bit before we loop around and do it again
//		delay(10);
//	}
}
